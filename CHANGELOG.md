# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Fixed

-   [Accessibility] Fix insufficient labels on sub menus
-   [Accessibility] Fix unreachable show more link
-   [Accessibility] Add unique titles to pages
-   [Accessibility] Fix insufficient contrast in primary color
-   [Accessibility] Fix current address not read by screen readers
-   [Accessibility] Fix sub menu options not being usable with screen reader or keyboard
-   [Accessibility] Fix search returning nothing with an empty search
-   [Accessibility] Add missing search landmark
-   [Accessibility] Fix service info button for keyboard and screen reader users
-   [Accessibility] Add contentinfo landmark
-   [Accessibility] Hide map markers from some screen reader navigation approaches so that the application is easier to browse through

## [1.1.2] - 2021-01-05

### Changed

-   [#34](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/34) Run tests in GitHub
-   [#37](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/34) Use prettier to format project
-   [#45](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/45) Show all opening hours
-   [#47](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/47) Use icon instead of tooltip for outbound link

### Fixed

-   [#36](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/36) [Accessibility] HTML document language is now synced with application language
-   [#36](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/36) [Accessibility] Language toggles now have the correct lang attribute
-   [#38](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/38) [Accessibility] Make unit modal closable with keyboard
-   [#39](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/39) [Accessibility] Add text label to unit modal close link
-   [#40](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/40) [Accessibility] Info button not reachable with keyboard
-   [#41](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/41) [Accessibility] Make search button accessible with keyboard and move it after the search field
-   [#43](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/43) [Accessibility] Add accessible names to map and list buttons
-   [#44](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/44) [Accessibility] Warn when links open a new window
-   [#42](https://github.com/City-of-Helsinki/outdoors-sports-map/pull/42) [Accessibility] Add jump link
